#!/bin/bash

# display network load:
#   transmitted bytes in each second, accumulated values at the exit
#   ./ifload.sh interface

export LANG=C

# signal handler
trap 'echo ""; echo RX_accum\: $RX_accum; echo TX_accum\: $TX_accum; exit' 2

[ -z $1 ] && echo "ifload.sh ethN" && exit 1

# initalize the values
INTERFACE=$1
RX=`/sbin/ifconfig $INTERFACE | grep "RX bytes" | sed -e "s/.*RX\ bytes\:\([0-9]*\).*/\1/"`
TX=`/sbin/ifconfig $INTERFACE | grep "TX bytes" | sed -e "s/.*TX\ bytes\:\([0-9]*\).*/\1/"`
RX_accum=0
TX_accum=0

while [ true ]; do
    # sleep 1 second
    sleep 1
    
    # renew the values
    RX_old=$RX
    TX_old=$TX
    RX=`/sbin/ifconfig $INTERFACE | grep "RX bytes" | sed -e "s/.*RX\ bytes\:\([0-9]*\).*/\1/"`
    TX=`/sbin/ifconfig $INTERFACE | grep "TX bytes" | sed -e "s/.*TX\ bytes\:\([0-9]*\).*/\1/"`
    RX_diff=`echo "$RX - $RX_old" | bc`
    TX_diff=`echo "$TX - $TX_old" | bc`
    RX_accum=`echo "$RX_accum + $RX_diff" | bc`
    TX_accum=`echo "$TX_accum + $TX_diff" | bc`

    # print
    echo "RX: $RX_diff"
    echo "TX: $TX_diff"
    echo ""
done
